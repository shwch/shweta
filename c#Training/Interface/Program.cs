﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            //Employee e = new Employee();
            //e.Bless();
            //e.GetName();

            IPerson ip = new Employee();
            IGod ig = new Employee();

            ip.GetName();
            ig.GetName();
            Console.ReadKey();
        }
    }
}
